﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SortingChallenge.UnitTests
{
    [TestClass]
    [TestCategory("Baseline implementation")]
    public class BaselineSortingTests : SortingUnitTestsBase<BaselineAlgorithm>
    {

    }

    [TestClass]
    [TestCategory("New implementation")]
    public class SortingTests : SortingUnitTestsBase<SortingAlgorithm>
    {
        
    }
}