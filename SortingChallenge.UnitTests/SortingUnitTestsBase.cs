﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SortingChallenge.UnitTests
{
    public abstract class SortingUnitTestsBase<T> where T: ISortingAlgorithm, new ()
    {
        private readonly T algorithm;

        protected SortingUnitTestsBase()
        {
            this.algorithm = new T();
        }

        [TestMethod]
        public void EmptyArray()
        {
            var data = Array.Empty<int>();
            var result = this.algorithm.Sort(data);
            CheckSortingResult(data, result);
        }

        [TestMethod]
        public void SingleValue()
        {
            var data = new[] { 0 };
            var result = this.algorithm.Sort(data);
            CheckSortingResult(data, result);
        }

        [TestMethod]
        public void TwoValues()
        {
            var data = new[] { 0, -1000 };
            var result = this.algorithm.Sort(data);
            CheckSortingResult(data, result);
        }

        [TestMethod]
        public void PartiallySorted()
        {
            var data = new[] { 0, 1, 2, 3, 4, -1 };
            var result = this.algorithm.Sort(data);
            CheckSortingResult(data, result);
        }

        [TestMethod]
        public void PartiallySorted2()
        {
            var data = new[] { 5, 0, 1, 2, 3, 4 };
            var result = this.algorithm.Sort(data);
            CheckSortingResult(data, result);
        }

        [TestMethod]
        public void MinMaxValues()
        {
            var data = new[] { int.MaxValue, int.MinValue };
            var result = this.algorithm.Sort(data);
            CheckSortingResult(data, result);
        }

        [TestMethod]
        public void HundredValues()
        {
            var data = new[]
            {
                -155, -587, -981, -159, -327, 518, 728, -999, 703, -990, 106, -83, 573, 987, -546, 259, -428, 137, 609,
                -605, 483, -992, 449, 603, -834, -782, -530, 538, 237, -14, -230, -22, 646, -245, 708, 873, -37, -98,
                529, 741, 557, 176, 71, -26, -339, 925, -370, -665, 78, 560, 968, 726, -825, 68, -262, -699, -652, -251,
                775, 495, -968, 206, -650, -23, -76, 138, 258, -53, -313, 128, 293, -293, -814, -626, -703, 308, 793,
                -850, -874, 715, -415, 454, 974, 905, 347, 623, 86, -112, -812, 807, 781, -660, -64, -309, -589, 932,
                -300, 988, 835, -133
            };
            var result = this.algorithm.Sort(data);
            CheckSortingResult(data, result);
        }

        [TestMethod]
        public void SubsequentSorts()
        {
            var data = new[]
            {
                123, 522, 904, 343, -42, -951, -665, 779, 879, -643, -52, -653, 317, 179, -860, 428, -798, -900, 505, -310, -886, 905, 687, 544, -496, 250, -500, 477, -952, 752, -18, -498, -504, 683, 312, -157, 886, -550, 996, 548, -737, 588, 436, 138, -265, 367, -570, 552, 829, -595, -247, 68, -410, -889, -896, -966, 81, -307, -385, 402, 652, 48, -256, -803, 325, 823, -467, 70, 889, -709, 709, 342, 825, -184, 954, -466, 936, -427, 788, -173, 208, 827, 168, 264, 65, 56, 851, -261, -879, -328, 569, 901, 895, 791, -950, -169, 1, -833, -898, -73
            };
            var result = this.algorithm.Sort(data);
            CheckSortingResult(data, result);
            
            data = new[]
            {
                82, 521, 612, 175, 286, -117, 162, -470, 684, -14, -884, -726, 829, 909, 512, -330, -398, 618, 76, -428, 41, -837, -474, -201, -134, -863, -293, -368, 506, 610, 751, -896, -19, -166, 703, -61, -696, 519, 444, -883, 653, -140, -890, -803, 881, 908, -953, -786, -891, 477, -6, 395, -397, -2, -31, 752, 408, 201, -501, 844, 932, -148, 247, -941, -806, -589, 44, 905, -297, -843, -58, -705, 522, 474, -765, 2, 152, -822, -858, 181, 163, 271, -649, -20, -515, 761, -592, -411, -947, 852, -621, 958, -554, 462, -335, 560, 906, -176, -723, -972
            };
            result = this.algorithm.Sort(data);
            CheckSortingResult(data, result);
        }

        private static void CheckSortingResult<TData>(TData[] data, TData[] result) where TData : IComparable
        {
            Assert.AreEqual(data.Length, result.Length, "Výsledek nemá stejný počet prvků.");
            if (data.Length > 1)
            {
                Assert.IsTrue(IsSortedAscending(result),
                    "Výsledek není správně seřazený." + Environment.NewLine + FormatArray(result));
            }
        }

        private static bool IsSortedAscending<TData>(TData[] data) where TData: IComparable
        {
            var prev = data[0];
            for (var i = 1; i < data.Length; i++)
            {
                if (data[i].CompareTo(prev) < 0)
                {
                    return false;
                }
            }

            return true;
        }

        private static string FormatArray<TData>(IEnumerable<TData> data)
        {
            return "[" + string.Join(',', data) + "]";
        }
    }
}