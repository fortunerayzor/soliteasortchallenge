## Solitea Sorting Algorithm Challenge
Úkolem je doplnit implementaci metody `int[] Sort(int[] data)` v třídě `SortingAlgorithm`.

### Pravidla
1. Není dovoleno používat metody .NET Framworku, které jakkoliv souvisí s řazením
2. Není dovoleno používat externí knihovny, které obsahují řadící funkce
3. Všechny unit testy musí procházet - [spuštění unit testů ve Visual Studiu](https://docs.microsoft.com/en-us/visualstudio/test/getting-started-with-unit-testing?view=vs-2019&tabs=dotnet%2Cmstest#run-unit-tests)
4. Algoritmus musí projít na vstupech jako prázdné pole nebo pole o délce 2<sup>16</sup>.
5. Algoritmus musíte být schopný svými slovy vysvětlit.
6. Můžete použít kombinace algoritmů k dosáhnutí optimálního výkonu na různých délkách

K měření výkonu se používá knihovna BenchmarkDotNet, stačí pustit projekt v konfiguraci Release|AnyCPU. Vítězí algorimus s největším počtem výher z 10 vstupů (vstupy jsou pro všechny stejné). Při stejném počtu vítězných bodů vyhrává člověk s kratší celkovou dobou běhu algoritmů. 