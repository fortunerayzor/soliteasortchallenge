﻿using System;

namespace SortingChallenge
{
    public sealed class BaselineAlgorithm : ISortingAlgorithm
    {
        public int[] Sort(int[] data)
        {
            if (data == null || data.Length == 0)
            {
                return Array.Empty<int>();
            }

            Array.Sort(data, 0, data.Length);
            return data;
        }
    }
    
}
