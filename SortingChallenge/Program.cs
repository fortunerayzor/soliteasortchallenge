﻿using BenchmarkDotNet.Running;

namespace SortingChallenge
{
    internal class Program
    {
        private static void Main()
        {
            //var summary = BenchmarkRunner.Run<SortingBenchmark<BaselineAlgorithm>>();
            var summary = BenchmarkRunner.Run<SortingBenchmark<SortingAlgorithm>>();
        }
    }
}
