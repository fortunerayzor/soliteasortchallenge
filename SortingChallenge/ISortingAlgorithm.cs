﻿namespace SortingChallenge
{
    public interface ISortingAlgorithm
    {
        int[] Sort(int[] data);
    }
}