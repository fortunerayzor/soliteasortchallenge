﻿using System;
using System.Collections.Generic;
using BenchmarkDotNet.Attributes;

namespace SortingChallenge
{
    using System.Linq;

    using BenchmarkDotNet.Engines;

    [MemoryDiagnoser]
    [HtmlExporter]
    [RPlotExporter]
    [SimpleJob(RunStrategy.Monitoring)]
    public class SortingBenchmark<T> where T: ISortingAlgorithm, new()
    {
        private const int Seed = 42;

        private const int DataStart = 0;

        private const int DataEnd = 16;

        private readonly T sortingAlgorithm;

        private readonly Dictionary<int, int[]> dataDictionary = new();

        public SortingBenchmark()
        {
            this.sortingAlgorithm = new T();
        }

        [ParamsSource(nameof(GetDataRange))]
        public int DataSize { get; set; }

        public IEnumerable<int> GetDataRange() => Enumerable.Range(DataStart, DataEnd - DataStart + 1);

        [GlobalSetup]
        public void Setup()
        {
            var rnd = new Random(Seed);
            foreach (var powerOf in this.GetDataRange())
            {
                var arraySize = 1 << powerOf;
                var data = new int[arraySize];
                for (var i = 0; i < arraySize; i++)
                {
                    data[i] = rnd.Next();
                }

                this.dataDictionary[powerOf] = data;
            }
        }

        [Benchmark]
        public void Bench()
        {
            this.sortingAlgorithm.Sort(this.dataDictionary[this.DataSize]);
        }
    }
}